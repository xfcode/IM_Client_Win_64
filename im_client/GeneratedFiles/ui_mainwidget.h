/********************************************************************************
** Form generated from reading UI file 'mainwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWIDGET_H
#define UI_MAINWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>
#include "drawerlistwidget.h"

QT_BEGIN_NAMESPACE

class Ui_MainWidget
{
public:
    QWidget *widget;
    QWidget *widget_4;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QListView *msg_list;
    QWidget *page_2;
    DrawerListWidget *friend_list;
    QWidget *widget_2;
    QPushButton *close;
    QWidget *widget_3;
    QTextEdit *textEdit;
    QWidget *widget_5;
    QWidget *widget_6;
    QPushButton *pushButton;

    void setupUi(QDialog *MainWidget)
    {
        if (MainWidget->objectName().isEmpty())
            MainWidget->setObjectName(QStringLiteral("MainWidget"));
        MainWidget->resize(725, 501);
        widget = new QWidget(MainWidget);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(10, 70, 231, 421));
        widget_4 = new QWidget(widget);
        widget_4->setObjectName(QStringLiteral("widget_4"));
        widget_4->setGeometry(QRect(10, 10, 211, 51));
        stackedWidget = new QStackedWidget(widget);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        stackedWidget->setGeometry(QRect(10, 70, 201, 331));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(stackedWidget->sizePolicy().hasHeightForWidth());
        stackedWidget->setSizePolicy(sizePolicy);
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        msg_list = new QListView(page);
        msg_list->setObjectName(QStringLiteral("msg_list"));
        msg_list->setGeometry(QRect(0, 10, 201, 321));
        msg_list->setStyleSheet(QLatin1String(" QListView::item:selected {\n"
"      border: 1px solid #6a6ea9;\n"
"  }"));
        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        friend_list = new DrawerListWidget(page_2);
        friend_list->setObjectName(QStringLiteral("friend_list"));
        friend_list->setGeometry(QRect(0, 10, 201, 321));
        stackedWidget->addWidget(page_2);
        widget_2 = new QWidget(MainWidget);
        widget_2->setObjectName(QStringLiteral("widget_2"));
        widget_2->setGeometry(QRect(10, 10, 701, 51));
        widget_2->setStyleSheet(QStringLiteral("background-color: rgb(0, 85, 255);"));
        close = new QPushButton(widget_2);
        close->setObjectName(QStringLiteral("close"));
        close->setGeometry(QRect(653, 8, 41, 35));
        close->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255,0);"));
        widget_3 = new QWidget(MainWidget);
        widget_3->setObjectName(QStringLiteral("widget_3"));
        widget_3->setGeometry(QRect(250, 70, 471, 421));
        textEdit = new QTextEdit(widget_3);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(10, 310, 451, 61));
        widget_5 = new QWidget(widget_3);
        widget_5->setObjectName(QStringLiteral("widget_5"));
        widget_5->setGeometry(QRect(10, 269, 451, 31));
        widget_6 = new QWidget(widget_3);
        widget_6->setObjectName(QStringLiteral("widget_6"));
        widget_6->setGeometry(QRect(10, 10, 451, 251));
        pushButton = new QPushButton(widget_3);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(354, 382, 81, 31));

        retranslateUi(MainWidget);

        stackedWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWidget);
    } // setupUi

    void retranslateUi(QDialog *MainWidget)
    {
        MainWidget->setWindowTitle(QApplication::translate("MainWidget", "MainWidget", nullptr));
        close->setText(QApplication::translate("MainWidget", "\345\205\263\351\227\255", nullptr));
        pushButton->setText(QApplication::translate("MainWidget", "\345\217\221\351\200\201", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWidget: public Ui_MainWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWIDGET_H
