﻿#include "drawerlistwidget.h"
#include "debug.h"
#include <QVariant>
#include "accountdata.h"
#include "eventtype.h"
#include <cassert>



static QListWidgetItem * makeFriendListItem(QListWidget * w, AccountBaseInfor * infor)
{
	QListWidgetItem * item = new QListWidgetItem(w, QListWidgetItem::UserType);

	AccountItemData d(infor, AccountItemData::PaintFlag::AcItem);
	QVariant v;
	v.setValue(d);
	item->setData(0, v);

	return item;
}

static QListWidgetItem * makeFriendListItem_bar(QListWidget * w,const QString & gname)
{
	QListWidgetItem * item = new QListWidgetItem(w, QListWidgetItem::UserType);

	AccountItemData d(gname, AccountItemData::FoldBar);

	QVariant v;
	v.setValue(d);
	item->setData(0, v);

	return item;
}


DrawerListWidget::DrawerListWidget(QWidget *parent)
	: QListWidget(parent)
{
	m_friend_list_data = AcountDataBuffer::getInstance()->get_friend_map();

	if (m_friend_list_data)
	{
		Debug("初始化friendList\n");


		for each (auto var in *m_friend_list_data)
		{
			this->addItem(makeFriendListItem_bar(this, QString::fromStdString(var.first)));
			for (auto v:var.second)
			{
				this->addItem(makeFriendListItem(this, v->m_base_infor));
			}
		}
	}
}


DrawerListWidget::~DrawerListWidget()
{

}

void DrawerListWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button()==Qt::MouseButton::LeftButton)
	{
		auto item = itemAt(event->pos());
		
		if (item)
		{
			AccountItemData d = item->data(Qt::DisplayRole).value<AccountItemData>();
			if (d.flag==AccountItemData::AcItem)
			{
				Debug("data: %s\n", ((AccountBaseInfor *)d.infor)->m_ac_number.toStdString().c_str());
			}
		}

	}


	QListWidget::mousePressEvent(event);
}

void DrawerListWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
	if (event->button()==Qt::MouseButton::LeftButton)
	{
		auto item = itemAt(event->pos());
		if (item)
		{
			AccountItemData d = item->data(Qt::DisplayRole).value<AccountItemData>();
			if (d.flag==AccountItemData::FoldBar)
			{
				Debug("data: bar  %s\n",d.bar_name.toStdString().c_str());
			}
		}
	}
	QListWidget::mouseDoubleClickEvent(event);
}

bool DrawerListWidget::event(QEvent * e)
{
	return QListWidget::event(e);
}

