#include "login.h"
#include <QtWidgets/QApplication>
#include "mainwidget.h"

//#define TEST


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

#ifdef TEST
	MainWidget mainw;
	mainw.show();
	return a.exec();

#else
	Login w;
	w.show();
	if (w.exec() == QDialog::Accepted)
	{
		MainWidget mainw;
		mainw.show();
		return a.exec();
	}
	return 0;
#endif
	
}
