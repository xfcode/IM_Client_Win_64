﻿#include "msglistmodel.h"

#include "msglist.h"


MsgListModel::MsgListModel(QObject *parent)
	: QAbstractListModel(parent)
{
	m_msg_list = MsgList::getInstance();
	for (int i=0;i<10;i++)
	{
		MsgListItemData d;
		d.msg_list_item_id = m_msg_list->getNextItemId();
		d.name = QString::fromLocal8Bit("测试群");
		d.msg_tag = QString::fromLocal8Bit("在爱爱爱ii萨克的时间好快");
		m_msg_list->addItem(d);
	}

}

MsgListModel::~MsgListModel()
{
}



QVariant MsgListModel::data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const
{
	if (!index.isValid())
	{
		return QVariant();
	}

	QVariant v;
	v.setValue<MsgListItemData>(m_msg_list->getItemDataByIndex(index.row()));
	return v;
}

int MsgListModel::rowCount(const QModelIndex &parent /* = QModelIndex() */) const
{
	return m_msg_list->getSize();
}
