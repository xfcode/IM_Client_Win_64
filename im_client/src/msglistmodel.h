#pragma once
#include <QAbstractListModel> 
#include <QVariant>


class MsgList;

class MsgListModel : public QAbstractListModel
{
	Q_OBJECT

public:
	MsgListModel(QObject *parent);
	~MsgListModel();

	virtual QVariant data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const;
	virtual int rowCount(const QModelIndex &parent /* = QModelIndex() */) const;

private:

	MsgList * m_msg_list;
};
