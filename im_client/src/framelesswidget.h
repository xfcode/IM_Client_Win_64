﻿#pragma once

#include <QDialog>
#include <QMouseEvent>
#include <QDebug>

class FramelessWidget : public QDialog
{
	Q_OBJECT

protected:

	/******窗口拖动******/
	bool m_dragging; //是否移动 
	QPoint m_startPos;  //拖动的开始坐标 
	QPoint m_framePos;   //窗体的原始坐标 
	int    m_barHeight;    //bar 的高度  
	virtual void mouseMoveEvent(QMouseEvent * event);
	virtual void mousePressEvent(QMouseEvent * event);
	virtual void mouseReleaseEvent(QMouseEvent * event);

	void setBarHeight(int h) { m_barHeight = h; }

public:
	FramelessWidget(QWidget *parent);
	~FramelessWidget();
};
