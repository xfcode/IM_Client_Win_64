﻿#pragma once

#include <QtWidgets/QDialog>
#include "ui_login.h"
#include "framelesswidget.h"
#include <QEvent>


//登陆成功
#define LoginRsultCode_sucess 0
//账号或者密码错误 
#define LoginRsultCode_None   1
//禁止重复登陆
#define LoginRsultCode_Repetition 2

class NetEventDispatch;

class Login : public FramelessWidget
{
	Q_OBJECT

public:
	Login(QWidget *parent = Q_NULLPTR);

private :

	void login();

	virtual bool event(QEvent *event);

private:
	Ui::LoginClass ui;
	NetEventDispatch *  m_net_dispatch;
	QString   m_ac_muber;
};
