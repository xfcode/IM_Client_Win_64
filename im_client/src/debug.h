#pragma once
#include <cstdio>

#ifdef _DEBUG

#define Debug(arg, ...) do \
{  \
	printf("[DEBUG] func : %s \n",__FUNCTION__); printf(arg, __VA_ARGS__ );    \
} while (0)

#else

#define Debug   

#endif
