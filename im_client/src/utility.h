﻿
#ifndef Utility_h__
#define Utility_h__

#include <cstdint>


#define QT_SINGLETON_PATTERN(className)\
		private:\
		className(const className & o) = delete;\
		className & operator=(const className & o) = delete; \
		public:  \
		static className * getInstance() { static className m(nullptr); return &m; }

/************************************************************************/
/*
一些小工具 ：

1.字节序转换


*/
/************************************************************************/

namespace Utility
{

	/************************************************************************/
	/* 字节序转换                                                                        */
	/************************************************************************/

	/*
	@buf   存放转换后的字节
	@val   小端的数值
	*/

	inline void ilittle_to_big_endian_16(uint8_t * buf, uint16_t val)
	{
		buf[0] = (uint8_t)(val >> 8);
		buf[1] = (uint8_t)val;
	}

	inline uint16_t big_to_ilittle_endian_16(uint8_t * buf)
	{
		return (buf[0] << 8) | buf[1];
	}

	inline void ilittle_to_big_endian_32(uint8_t * buf, uint32_t val)
	{
		buf[0] = (uint8_t)(val >> 24);
		buf[1] = (uint8_t)(val >> 16);
		buf[2] = (uint8_t)(val >> 8);
		buf[3] = (uint8_t)val;
	}

	inline uint32_t big_to_ilittle_endian_32(uint8_t * buf)
	{
		return buf[0] << 24 | buf[1] << 16 | buf[2] << 8 | buf[3];
	}

	/*
	测试代码

	int a = 10;
	unsigned short b = 100;
	unsigned char buff[2];
	unsigned char  buf[4];
	Utility::ilittle_to_big_endian_32(buf, a);
	int d=Utility::big_to_ilittle_endian_32(buf);
	cout << d << endl;

	Utility::ilittle_to_big_endian_16(buff, b);
	int c=Utility::big_to_ilittle_endian_16(buff);
	cout << c<< endl;


	*/




}


#endif // Utility_h__