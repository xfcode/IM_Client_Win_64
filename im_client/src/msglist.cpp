#include "msglist.h"
#include <assert.h>

MsgList::MsgList(QObject *parent)
	: QObject(parent)
{
	m_msg_list_data = new QList<MsgListItemData>();

}

MsgList::~MsgList()
{
}

int MsgList::addItem(const MsgListItemData & data)
{
	m_msg_list_data->push_back(data);
	return data.msg_list_item_id;
}

void MsgList::removeItem(int id)
{
	QList<MsgListItemData>::iterator iter = m_msg_list_data->begin();

	for (;iter!=m_msg_list_data->end();iter++)
	{
		if ((*iter).msg_list_item_id==id)
		{
			m_msg_list_data->erase(iter);
			return;
		}
	}
}


const MsgListItemData MsgList::getItemDataById(int id)
{
	for (auto v : *m_msg_list_data)
	{
		if (v.msg_list_item_id == id)
		{
			return v;
		}
	}

	assert(false);
}

const MsgListItemData MsgList::getItemDataByIndex(int Index)
{
	return m_msg_list_data->at(Index);
}

int MsgList::m_msg_lsit_item_id=0;

int MsgList::getNextItemId()
{
	++m_msg_lsit_item_id;
	return m_msg_lsit_item_id;
}
