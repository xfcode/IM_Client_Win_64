#include "eventtype.h"

QEvent::Type UEvent::loginEvent = static_cast<QEvent::Type>(QEvent::registerEventType());;

QEvent::Type UEvent::connectFailEvent = static_cast<QEvent::Type>(QEvent::registerEventType());;

QEvent::Type UEvent::getFriendListEvent = static_cast<QEvent::Type>(QEvent::registerEventType());;