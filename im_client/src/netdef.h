#pragma once
#include <QMetaType>

class NetEventPacket
{
public:
	uint16_t data_size;
	uint16_t type;
	char * data;
	NetEventPacket() :data(nullptr), type (-1),data_size(0){}
};


const uint16_t PktType_Login_request = 0;
const uint16_t PktType_Login_Respond = 1;
const uint16_t PktType_GetFriend_Request = 2;
const uint16_t PktType_GetFriend_Respond = 3;



const uint16_t PktType_ConnectSucces = 100;
const uint16_t PktType_ConnectFail = 101;