﻿#include "framelesswidget.h"


void FramelessWidget::mouseMoveEvent(QMouseEvent * event)
{
	if (event->buttons() & Qt::LeftButton)   //注意buttons（）    
	{
		if (m_dragging)
		{
			if (event->pos().y() < m_barHeight)
			{
				QPoint data = event->globalPos() - m_startPos;
				qDebug() << data;
				move(m_framePos + data);
			}
		}
	}
	QWidget::mouseMoveEvent(event);
}

void FramelessWidget::mousePressEvent(QMouseEvent * event)
{
	if (event->button() == Qt::LeftButton)
	{
		if (event->pos().y() < m_barHeight)
		{
			m_dragging = true;
			m_startPos = event->globalPos();
			m_framePos = frameGeometry().topLeft();
		}
	}
	QWidget::mousePressEvent(event);
}

void FramelessWidget::mouseReleaseEvent(QMouseEvent * event)
{
	if (event->buttons() & Qt::LeftButton)
	{
		m_dragging = false;
	}
	QWidget::mouseReleaseEvent(event);
}

FramelessWidget::FramelessWidget(QWidget *parent)
	: QDialog(parent)
{
	m_barHeight = 40;
	setWindowFlags(Qt::FramelessWindowHint);
}

FramelessWidget::~FramelessWidget()
{
}
