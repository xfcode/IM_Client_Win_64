#pragma once

#include <QObject>
#include <QMap>
#include <QList>
#include "utility.h"

class MsgListItemData 
{
public:
	int msg_list_item_id;
	QString name;
	QString msg_tag;
};
Q_DECLARE_METATYPE(MsgListItemData)

class MsgList : public QObject
{
	Q_OBJECT

	MsgList(QObject *parent);
	~MsgList();

	QT_SINGLETON_PATTERN(MsgList)

public:
	int getNextItemId();

	int addItem(const MsgListItemData & data);
	void removeItem(int id);
	const MsgListItemData getItemDataById(int id);
	const MsgListItemData getItemDataByIndex(int Index);
	int getSize() { return m_msg_list_data->size(); }

private:
	QList<MsgListItemData>     *m_msg_list_data;
	static int m_msg_lsit_item_id;

private:
};
