﻿#pragma once

#include <QObject>
#include <QVariant>
#include "utility.h"
#include <map>
#include <vector>
#include <string>


enum Line_state
{
	OnLine, OffLine
};

struct AccountBaseInfor
{
	//备注信息
	QString m_notes;
	//账号
	QString m_ac_number;
	//在线状态
	Line_state m_state;
	//昵称
	QString m_name;

};


struct AccountNetInfor
{

};


class AccountData : public QObject
{
	Q_OBJECT

public:
	AccountData();
	~AccountData();

public:
	AccountBaseInfor *m_base_infor;
	AccountNetInfor * m_net_infor;
};


class AccountItemData
{
public:
	enum PaintFlag
	{
		AcItem,FoldBar
	};

	QString bar_name;
	void * infor;
	
	PaintFlag flag;

	AccountItemData() {}
	AccountItemData(void * data,PaintFlag flag) 
	 :infor(data),flag(flag){}
	AccountItemData(const QString & s,PaintFlag flag)
	:bar_name(s),flag(flag),infor(nullptr){}

	~AccountItemData() {}
};

Q_DECLARE_METATYPE(AccountItemData)

/******************************************/
//类名：AcountDataBuffer
//创建时间 ：2018/5/21
//类型：
//描述：describe
/******************************************/
typedef std::vector<AccountData *> AcdataVec;
typedef std::map<std::string, AcdataVec> GNmaeMapAcVec;
typedef std::map<std::string , AccountData *> IdMapAcData;
class AcountDataBuffer
	:public QObject
{
	Q_OBJECT
	
		QT_SINGLETON_PATTERN(AcountDataBuffer)

private:
	AcountDataBuffer(QObject * );
	~AcountDataBuffer();

	virtual bool event(QEvent *e);

public:
	GNmaeMapAcVec * get_friend_map();

signals:
	void dataRead();

private:
	//分组名到  ac vector 的映射 
	GNmaeMapAcVec*  m_group_ac_map; 
	//账号到 ac 的映射
	IdMapAcData   *  m_Id_map;
};