#include "mainwidget.h"
#include "msglistdelegate.h"
#include "msglistmodel.h"
#include "friendlistdelegate.h"
#include "accountdata.h"
#include <QVariant>
#include <QListWidget>
#include "neteventdispatch.h"

MainWidget::MainWidget(QWidget *parent)
	: FramelessWidget(parent)
{
	ui.setupUi(this);

	
	MsgListModel  * m = new MsgListModel(ui.msg_list);
	MsgListDelegate * d = new MsgListDelegate(ui.msg_list);
	

	ui.msg_list->setModel(m);
	ui.msg_list->setItemDelegate(d);

	connect(ui.close, &QPushButton::clicked, [&]() {this->close(); });

	ui.friend_list->setItemDelegate(new FriendListDelegate(ui.friend_list));
}

MainWidget::~MainWidget()
{
}



