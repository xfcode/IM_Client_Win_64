﻿#pragma once
#include <QObject>
#include <QEvent>

class UEvent
{
public:
	static QEvent::Type loginEvent;
	static QEvent::Type connectFailEvent;
	static QEvent::Type getFriendListEvent;
};

class LoginEvent:
	public QEvent
{
public:
	LoginEvent(Type type):QEvent(type){}
	~LoginEvent(){}

	QString token;

	int   result;
};

/******************************************/
//类名：FriendLsitEvent
//创建时间 ：2018/5/21
//类型：event
//描述：describe
/******************************************/

class AccountData;

class FriendLsitEvent:public QEvent
{
public:
	FriendLsitEvent(Type type) :QEvent(type) {}
	
	~FriendLsitEvent() {}

	std::string data_json;
private:
	
};