﻿#pragma once

#include <QObject>

#include <QMap>

#include <QLinkedList>

#include <QEvent>

#include "utility.h"
#include <QThread>
#include <QWaitCondition>
#include <QMutex>
#include "netdef.h"



//网络事件派发层    屏蔽具体的通信细节  

typedef  QLinkedList<QObject *> QObjLink;
typedef QMap<int, QObjLink> EventMapObj;

class TcpNetWork;

class NetEventDispatch : public QThread
{
	Q_OBJECT

		//单例  
	QT_SINGLETON_PATTERN(NetEventDispatch)


	NetEventDispatch(QObject *parent);
	~NetEventDispatch();

public:

	//注册订阅者关心的事件  
	/*
	@event_type   关心的事件  
	@subscriber   订阅者  
	*/
	void  registerEvent(int event_type, QObject * subscriber);
	void  startNetWork();
	void  startDispatch();

	TcpNetWork * getTcpNetWorkInstance();
private:
	void dispatch(NetEventPacket & pkt);
	virtual void run();
	void  recvEvent(NetEventPacket pkt);
private:
	QMutex m_mutex;
	EventMapObj m_event_map;
	bool   m_quite;
	QList<NetEventPacket> m_event_data_buffer;
	QWaitCondition   m_cond;
	QMutex    m_cond_mutex;
	TcpNetWork * m_tcp_net_work;
};
