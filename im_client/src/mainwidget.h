#pragma once

#include <QDialog>
#include "ui_mainwidget.h"
#include "framelesswidget.h"
#include <QWidget>  
#include <QLabel>  
#include <QEvent>  

class NetEventDispatch;

class MainWidget : public FramelessWidget
{
	Q_OBJECT

public:
	MainWidget(QWidget *parent = Q_NULLPTR);
	~MainWidget();

private:
	Ui::MainWidget ui;


	NetEventDispatch *  m_net_dispatch;
};

