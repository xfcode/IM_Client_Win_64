﻿#pragma once

#include <QListWidget>
#include <QStyledItemDelegate>
#include <QMouseEvent>
#include <QMenu>
#include <QList>
#include <QEvent>
#include <map>
/************************************************************************/
/* 抽屉列表                                                             */
/************************************************************************/

class AccountData;

typedef std::vector<AccountData *> AcdataVec;
typedef std::map<std::string, AcdataVec> GNmaeMapAcVec;
typedef std::map<std::string, AccountData *> IdMapAcData;

class AccountData;

class DrawerListWidget : public QListWidget
{
	Q_OBJECT

public:
	DrawerListWidget(QWidget *parent);
	~DrawerListWidget();

	virtual void  mousePressEvent(QMouseEvent *event);
	virtual void  mouseDoubleClickEvent(QMouseEvent *event);

	virtual bool event(QEvent *e);
private:

	  GNmaeMapAcVec *m_friend_list_data;
};




