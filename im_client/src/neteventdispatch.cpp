﻿#include "neteventdispatch.h"
#include "IM.pb.h"
#include "eventtype.h"
#include <QCoreApplication>
#include "NetWork.h"
#include "accountdata.h"


NetEventDispatch::NetEventDispatch(QObject *parent)
	: QThread(parent),m_quite(false)
	,m_tcp_net_work(nullptr)
{
	qRegisterMetaType<NetEventPacket>("NetEventPacket");
}

NetEventDispatch::~NetEventDispatch()
{
}

void NetEventDispatch::registerEvent(int event_type, QObject * subscriber)
{
	EventMapObj::iterator iter = m_event_map.find(event_type);
	if (iter!=m_event_map.end())
	{
		(*iter).append(subscriber);
	}
	else
	{
		QObjLink link;
		link.append(subscriber);
		m_event_map.insert(event_type, link);
	}
}


void NetEventDispatch::startNetWork()
{
	m_tcp_net_work = new TcpNetWork();
	connect(m_tcp_net_work, &TcpNetWork::ConnectServerFail, this,&NetEventDispatch::recvEvent);
	connect(m_tcp_net_work, &TcpNetWork::recvData, this, &NetEventDispatch::recvEvent);

	m_tcp_net_work->startWork("192.168.12.138", 8010);
}

void NetEventDispatch::startDispatch()
{
	m_quite = false;

	if (!this->isRunning())
	{
		start();
	}
}

TcpNetWork * NetEventDispatch::getTcpNetWorkInstance()
{
	assert(m_tcp_net_work);
	return m_tcp_net_work;
}

void NetEventDispatch::dispatch(NetEventPacket & pkt)
{
	switch (pkt.type)
	{
		case PktType_Login_Respond:      //登陆  
		{
			auto f = m_event_map.find(UEvent::loginEvent);
			if (f == m_event_map.end())
			{
				return;
			}
			IM_Protocol::LoginRespond login_respond;
			login_respond.ParseFromArray(pkt.data, pkt.data_size);
			LoginEvent * e = new LoginEvent(UEvent::loginEvent);
			e->result = login_respond.rcode();
			if (e->result==0)
			{
				e->token = QString::fromStdString(login_respond.token());
				Debug("token %s\n", login_respond.token().c_str());
			}
			for (auto v : *f)
			{
				QCoreApplication::postEvent(v, e);
			}
		}
		break;

		case PktType_ConnectFail:  //服务器连接失败  
		{
			
			auto f = m_event_map.find(UEvent::connectFailEvent);
			if (f==m_event_map.end())
			{
				return;
			}
			QEvent * e = new QEvent(UEvent::connectFailEvent);
			for (auto v : *f)
			{
				QCoreApplication::postEvent(v, e);
			}
		}
		break;

		case PktType_GetFriend_Respond:
		{
			auto f = m_event_map.find(UEvent::getFriendListEvent);
			if (f == m_event_map.end())
			{
				return;

			}
			FriendLsitEvent * e = new FriendLsitEvent(UEvent::getFriendListEvent);
			IM_Protocol::FriendRespond r;
			r.ParseFromArray(pkt.data, pkt.data_size);
			if (r.rcode()!=0)
			{
				Debug("获取好友列表失败\n");
				return;
			}
			e->data_json = r.data();
			
			for (auto v : *f)
			{
				QCoreApplication::postEvent(v, e);
			}
		}
		break;
		default:
			break;
	}
}

void NetEventDispatch::run()
{
	while (!m_quite)
	{
		if (m_event_data_buffer.size()>0)
		{
			m_mutex.lock();
			NetEventPacket pkt = m_event_data_buffer.front();
			m_event_data_buffer.pop_front();
			m_mutex.unlock();


			//分析包 

			dispatch(pkt);
		}
		else
		{
			m_cond_mutex.lock();
			m_cond.wait(&m_cond_mutex,2000);
			m_cond_mutex.unlock();
		}
	}
}

void NetEventDispatch::recvEvent(NetEventPacket pkt)
{
	{
		QMutexLocker locker(&m_mutex);
		m_event_data_buffer.push_back(pkt);
	}
	m_cond.wakeOne();
}

