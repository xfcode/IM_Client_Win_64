﻿#include "friendlistdelegate.h"
#include "debug.h"
#include "accountdata.h"


FriendListDelegate::FriendListDelegate(QObject *parent)
	: QStyledItemDelegate(parent)
{

}

FriendListDelegate::~FriendListDelegate()
{
}

void FriendListDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	//QStyleOptionViewItem op;
	//initStyleOption(&op, index);
	painter->save();

	AccountItemData d = index.data().value<AccountItemData>();

	if (d.flag==AccountItemData::FoldBar)
	{
		QRect des = option.rect;
		des.adjust(2, 2, -2, -2);

		painter->drawText(des,d.bar_name);

	}
	else
	{
		AccountBaseInfor * infor = (AccountBaseInfor *)d.infor;
		QRect  des = option.rect;
		des.adjust(5, 5, -5, -5);

		if (option.state & QStyle::State_MouseOver)
		{
			painter->setBrush(QColor(200, 200, 200));
			painter->drawRect(des);
			painter->setBrush(Qt::NoBrush);
		}


		painter->setPen(QColor(0, 0, 0));
		QRect head_portrait_rect = des;
		head_portrait_rect.setRight(head_portrait_rect.right() - 140);
		head_portrait_rect.adjust(0, 5, 0, -5);
		painter->drawPixmap(head_portrait_rect, QPixmap(":/Login/Resources/hearp.jpg"));

		QRect text_rect = des;
		text_rect.setLeft(text_rect.left() + 60);

		QRect name_rect = text_rect;
		name_rect.setBottom(name_rect.bottom() - 25);
		name_rect.setTop(name_rect.top() + 10);
		QFont font = painter->font();
		font.setFamily("Microsoft YaHei");
		font.setPointSize(12);
		painter->setFont(font);

		painter->drawText(name_rect, Qt::AlignLeft, 
			infor->m_notes.isEmpty() ? infor->m_name : infor->m_notes,
			nullptr);

		font.setPixelSize(11);
		painter->setFont(font);
		painter->setPen(QColor(130, 130, 130));
		QRect msg_rect = text_rect;
		msg_rect.setTop(msg_rect.top() + 35);
		painter->drawText(msg_rect,
			Qt::AlignLeft,
			infor->m_state==OffLine?QString::fromLocal8Bit("[离线]"):QString::fromLocal8Bit("[在线]"),
			nullptr);
	}

	painter->restore();
}

QSize FriendListDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	AccountItemData d = index.data().value<AccountItemData>();
	if (d.flag==AccountItemData::FoldBar)
	{
		return QSize(200,30);
	}
	else
	{
		return QSize(200, 70);
	}
}
