#pragma once

#include <QStyledItemDelegate>
#include <QPainter>


class FriendListDelegate : public QStyledItemDelegate
{
	Q_OBJECT

public:
	FriendListDelegate(QObject *parent);
	~FriendListDelegate();



	virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;


	virtual QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;

};


