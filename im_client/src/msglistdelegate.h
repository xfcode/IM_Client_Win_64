#pragma once

#include <QStyledItemDelegate>
#include <QPainter>
#include <QRect>
#include <QStyleOptionFrame>
#include <QApplication>
#include <QLabel>
#include <QColor>
#include <QPushButton>

class MsgListDelegate : public QStyledItemDelegate
{
	Q_OBJECT

public:
	MsgListDelegate(QObject *parent);
	~MsgListDelegate();



	//virtual QWidget * createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
	virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
	//virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;
	//virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
	virtual QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
	//virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

};


